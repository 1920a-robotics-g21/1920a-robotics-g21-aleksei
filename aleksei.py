import os
import sys
import threading
import time

import pyaudio
import rospy
from geometry_msgs.msg import Twist
from google.cloud import speech
from google.cloud.speech import enums, types
from playsound import playsound
from robotont_sensors.msg import LaserScanSplit
from six.moves import queue

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/user/google.json"

# Audio recording parameters
RATE = 16000
CHUNK = int(RATE / 10)  # 100ms

# Object for preparing microphone input for recognition


class MicrophoneStream(object):
    """Opens a recording stream as a generator yielding the audio chunks."""

    def __init__(self, rate, chunk):
        self._rate = rate
        self._chunk = chunk

        # Create a thread-safe buffer of audio data
        self._buff = queue.Queue()
        self.closed = True

    def __enter__(self):
        self._audio_interface = pyaudio.PyAudio()
        self._audio_stream = self._audio_interface.open(
            format=pyaudio.paInt16,
            # The API currently only supports 1-channel (mono) audio
            # https://goo.gl/z757pE
            channels=1, rate=self._rate,
            input=True, frames_per_buffer=self._chunk,
            # Run the audio stream asynchronously to fill the buffer object.
            # This is necessary so that the input device's buffer doesn't
            # overflow while the calling thread makes network requests, etc.
            stream_callback=self._fill_buffer,
        )

        self.closed = False

        return self

    def __exit__(self, type, value, traceback):
        self._audio_stream.stop_stream()
        self._audio_stream.close()
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)
        self._audio_interface.terminate()

    def _fill_buffer(self, in_data, frame_count, time_info, status_flags):
        """Continuously collect data from the audio stream, into the buffer."""
        self._buff.put(in_data)
        return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            # Use a blocking get() to ensure there's at least one chunk of
            # data, and stop iteration if the chunk is None, indicating the
            # end of the audio stream.
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            # Now consume whatever other data's still buffered.
            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)


# Thread for running laser scanner
class LaserScan(threading.Thread):

    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.distances = False
        self.distance_list = {}

    def camera_callback(self, data):
        self.distances = data
        self.distance_list["left"] = self.distances.leftMin
        self.distance_list["center"] = self.distances.centerMin
        self.distance_list["right"] = self.distances.rightMin

    def run(self):
        while True:
            rospy.Subscriber("/scan_to_distance", LaserScanSplit,
                             callback=self.camera_callback, queue_size=1)

    def get_distance(self):
        return self.distance_list

# Thread for moving the robot around and running voice recognition
class Aleksei(threading.Thread):

    loop_counter = 1
    cmd = False
    command_memory = "move around"
    movement_speed = 0.5
    language_code = 'en-US'

    gspeech_client = speech.SpeechClient()
    audio_config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=RATE,
        language_code=language_code)
    streaming_config = types.StreamingRecognitionConfig(
        config=audio_config,
        interim_results=True)

    laserscan_data = LaserScan(3, "LaserScanThread")

    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.distances = False
        self.distance_list = {}

        self.velocity_publisher = rospy.Publisher(
            '/robotont/cmd_vel', Twist, queue_size=10)

        self.velocity_msg = Twist()
        rospy.init_node('robotont_velocity_publisher', anonymous=True)

    def run(self):
        while True:
            print("Listening to speech...")
            self.process_speech()

    def rb_move_forward(self):

        self.velocity_msg.linear.x = 0.1
        self.velocity_publisher.publish(self.velocity_msg)
        self.velocity_msg.linear.x = 0

        camera_distances = self.laserscan_data.get_distance()
        if self.command_memory == "move forward":
            if camera_distances["center"] < 0.5 or camera_distances["right"] < 0.5 or camera_distances["left"] < 0.5:
                self.command_memory = "stop moving"
                self.cmd = None
                print("found wall, stopped")

            if type(self.cmd) is type(2) and self.command_memory == "move forward":
                self.loop_counter += 1
                if self.loop_counter == (130000)/100*self.cmd:
                    print("distance reached, stopped")
                    self.command_memory = "stop moving"
                    self.loop_counter = 0
            else:
                pass

    def rb_move_backward(self):
        self.velocity_msg.linear.x = -0.1
        self.velocity_publisher.publish(self.velocity_msg)
        self.velocity_msg.linear.x = 0

    def rb_turn_around(self):
        self.velocity_msg.angular.z = - self.movement_speed
        while self.loop_counter < 10000:
            self.velocity_publisher.publish(self.velocity_msg)
            self.loop_counter += 1
        self.velocity_msg.angular.z = 0
        self.loop_counter = 0
        self.command_memory = False

    def rb_turn_left(self):
        self.velocity_msg.angular.z = self.movement_speed
        self.velocity_publisher.publish(self.velocity_msg)
        self.velocity_msg.angular.z = 0

    def rb_turn_right(self):
        self.velocity_msg.angular.z = -self.movement_speed
        self.velocity_publisher.publish(self.velocity_msg)
        self.velocity_msg.angular.z = 0

    def rb_move_right(self):
        self.velocity_msg.linear.y = -0.1
        self.velocity_publisher.publish(self.velocity_msg)
        self.velocity_msg.linear.y = 0 
    
    def rb_move_left(self):
        self.velocity_msg.linear.y = 0.1
        self.velocity_publisher.publish(self.velocity_msg)
        self.velocity_msg.linear.y = 0

    def rb_drive_around_right(self):
        self.velocity_msg.linear.x = 0.1
        self.velocity_msg.angular.z = - self.movement_speed
        self.velocity_publisher.publish(self.velocity_msg)
        self.velocity_msg.linear.x = 0
        self.velocity_msg.angular.z = 0

    def rb_drive_around_left(self):
        self.velocity_msg.linear.x = 0.1
        self.velocity_msg.angular.z = self.movement_speed
        self.velocity_publisher.publish(self.velocity_msg)
        self.velocity_msg.linear.x = 0
        self.velocity_msg.angular.z = 0

    def rb_get_excited(self):
        self.velocity_msg.linear.y = -0.2
        while self.loop_counter < 1000:
            self.velocity_publisher.publish(self.velocity_msg)
            self.loop_counter += 1
        self.velocity_msg.linear.y = 0.2
        self.loop_counter = 0
        while self.loop_counter < 1000:
            self.velocity_publisher.publish(self.velocity_msg)
            self.loop_counter += 1
        self.velocity_msg.linear.y = 0
        self.loop_counter = 0



    def rb_stop(self):
        pass

    def rb_exit(self):
        sys.exit()

    def rb_dance(self):
        self.movement_speed = 2

        while self.loop_counter < 2000:
            self.rb_move_forward()
            self.loop_counter += 1
        self.loop_counter = 0

        while self.loop_counter < 2000:
            self.rb_move_backward()
            self.loop_counter += 1
        self.loop_counter = 0

        while self.loop_counter < 2000:
            self.rb_turn_left()
            self.loop_counter += 1
        self.loop_counter = 0

        self.movement_speed = 0.5

    def rb_go_hunting(self):

        self.velocity_msg.linear.x = 0.5
        while self.loop_counter < 20000:
            self.velocity_publisher.publish(self.velocity_msg)
            self.loop_counter += 1
        self.loop_counter = 0
        self.velocity_msg.linear.x = 0

        self.velocity_msg.angular.z = -0.05
        while self.loop_counter < 6000:
            self.loop_counter += 1
            self.velocity_publisher.publish(self.velocity_msg)
        self.velocity_msg.angular.z = 0
        self.loop_counter = 0

        self.velocity_msg.linear.x = 0.3
        while self.loop_counter < 6000:
            self.velocity_publisher.publish(self.velocity_msg)
            self.loop_counter += 1
        self.loop_counter = 0

        self.velocity_msg.linear.x = -0.05
        while self.loop_counter < 6000:
            self.velocity_publisher.publish(self.velocity_msg)
            self.loop_counter += 1
        self.loop_counter = 0
        self.velocity_msg.linear.x = 0

        self.velocity_msg.angular.z = 0.05
        while self.loop_counter < 6000:
            self.velocity_publisher.publish(self.velocity_msg)
            self.loop_counter += 1
        self.loop_counter = 0
        self.velocity_msg.angular.z = 0

        self.velocity_msg.linear.x = 0.3
        while self.loop_counter < 6000:
            self.velocity_publisher.publish(self.velocity_msg)
            self.loop_counter += 1
        self.loop_counter = 0

        self.velocity_msg.linear.x = 0
        self.command_memory = "rb_move_around"
        self.cmd = "continue"

    def rb_move_around(self):
        distances = self.laserscan_data.get_distance()
        detection_distance = 0.45

        if distances["left"] < detection_distance and distances["center"] < detection_distance:
            self.rb_turn_right()

        elif distances["center"] < detection_distance and distances["right"] < detection_distance:
            self.rb_turn_right()

        elif distances["left"] < detection_distance:
            self.rb_turn_right()

        elif distances["right"] < detection_distance:
            self.rb_turn_left()

        elif distances["center"] < detection_distance:
            self.rb_turn_left()

        else:
            self.rb_move_forward()

    def process_speech(self):
        processing_start_time = time.time()
        with MicrophoneStream(RATE, CHUNK) as stream:
            audio_generator = stream.generator()
            requests = (types.StreamingRecognizeRequest(audio_content=content)
                        for content in audio_generator)

            responses = self.gspeech_client.streaming_recognize(
                self.streaming_config, requests)
            for response in responses:
                if time.time() - processing_start_time > 240:
                    return
                if not response.results:
                    continue
                result = response.results[0]
                if result.is_final:
                    final_result = result.alternatives[0].transcript
                    print("final result:", final_result)
                    self.cmd = final_result.strip()

                    playsound("ding.wav")

    def process_commands(self):
        commands = {"move forward": "rb_move_forward",
                    "move forwards" : "rb_move_forward",
                    "go forward" : "rb_move_forward",
                    "move backwards": "rb_move_backward",
                    "move left" : "rb_move_left",
                    "move right" : "rb_move_right",
                    "drive around right" : "rb_drive_around_right",
                    "drive around left" : "rb_drive_around_left",
                    "get excited" : "rb_get_excited",
                    "stop moving": "rb_stop",
                    "turn around": "rb_turn_around",
                    "turn left": "rb_turn_left",
                    "turn right": "rb_turn_right",
                    "go autonomous": "rb_move_around",
                    "move around": "rb_move_around",
                    "start dancing": "rb_dance",
                    "go hunting": "rb_go_hunting",
                    "turn off": "rb_exit"}

        try:
            self.cmd = int(self.cmd.strip())
            if type(self.cmd) == type(int):
                print("hakkama sain")
        except:
            pass

        if self.cmd in commands:
            self.command_memory = self.cmd
            command = getattr(self, commands.get(self.command_memory))
            command()
        else:
            try:
                command = getattr(self, commands.get(self.command_memory))
                command()
            except:
                pass

# Thread for running current commands
class Command(threading.Thread):
    def __init__(self, threadID, name, object):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.oAleksei = object

    def run(self):
        print("Started Processing Commands")
        while True:
            self.oAleksei.process_commands()
